import { StoreService } from './shared/services/store.service';
import { SpecialtiesService } from './shared/services/specialties.service';
import { GroupsService } from './shared/services/groups.service';
import { FacultiesService } from './shared/services/faculties.service';
import { CoursesService } from './shared/services/courses.service';
import { StudentsListModule } from './students-list/students-list.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AuthModule } from './auth/auth.module';
import { RoutingModule } from './routing.module';
import { UsersService } from './shared/services/users.service';
import { AuthService } from './shared/services/auth.service';
import { StudentsService } from './shared/services/students.service';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AuthModule,
    RoutingModule,
    StudentsListModule
  ],
  providers: [
    UsersService,
    AuthService,
    StudentsService,
    CoursesService,
    FacultiesService,
    GroupsService,
    SpecialtiesService,
    StoreService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
