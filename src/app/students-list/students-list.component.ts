import { CoursesService } from './../shared/services/courses.service';
import { SpecialtiesService } from './../shared/services/specialties.service';
import { GroupsService } from './../shared/services/groups.service';
import { FacultiesService } from './../shared/services/faculties.service';
import { StudentsService } from './../shared/services/students.service';
import { Student } from './../shared/models/student.model';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'ngm-students-list',
  templateUrl: './students-list.component.html',
  styleUrls: ['./students-list.component.scss']
})
export class StudentsListComponent implements OnInit {

  studentsList: Student[];
  filteredList: Student[];
  facultiesList: any[];
  groupsList: any[];
  specialtiesList: any[];
  coursesList: any[];
  selectedCourse: number;
  selectedFaculty = '';
  selectrdGroup = '';
  selectedSpecialty = '';
  form: FormGroup;

  constructor(
    private studentsService: StudentsService,
    private facultiesService: FacultiesService,
    private groupsService: GroupsService,
    private specialtiesService: SpecialtiesService,
    private coursesService: CoursesService
  ) { }

  ngOnInit() {
    this.facultiesService.getFacultiesList()
    .subscribe( facultiesList => this.facultiesList = facultiesList);

    this.groupsService.getGroupsList()
      .subscribe( groupsList => this.groupsList = groupsList);

    this.specialtiesService.getSpecialitiesList()
      .subscribe( specialtiesList => this.specialtiesList = specialtiesList);

    this.studentsService.getStudentsList()
      .subscribe( studentsList => {
        this.studentsList = studentsList;
        this.filteredList = studentsList;
      });

    this.coursesList = this.coursesService.getCoursesList();

    this.form = new FormGroup({
      course: new FormControl(''),
      faculty: new FormControl(''),
      specialty: new FormControl(''),
      group: new FormControl('')
    });
  }

  getFacultyById(id) {
    const index = id - 1;

    return this.facultiesList[index].name;
  }

  getSpecialtyById(id) {
    const index = id - 1;

    return this.specialtiesList[index].name;
  }

  getGroupById(id) {
    const index = id - 1;

    return this.groupsList[index].name;
  }

  filter() {
    this.filteredList = this.studentsList;
    const courseValue = this.form.get('course').value;
    const facultyValue = this.form.get('faculty').value;
    const specialtyValue = this.form.get('specialty').value;
    const groupValue = this.form.get('group').value;

    if (courseValue !== '') {
      this.filterByCourse();
    }

    if (facultyValue !== '') {
      this.filterByFaculty();
    }

    if (specialtyValue !== '') {
      this.filterBySpecialty();
    }

    if (groupValue !== '') {
      this.filterByGroup();
    }
  }

  private filterByCourse() {
    const course = parseInt(this.form.get('course').value, 10);

    this.filteredList = this.filteredList.filter((student) => {
      return student.course === course;
    });
  }

  private filterByFaculty() {
    const faculty = parseInt(this.form.get('faculty').value, 10);

    this.filteredList = this.filteredList.filter((student) => {
      return student.facultyId === faculty;
    });
  }


  private filterBySpecialty() {
    const specialty = parseInt(this.form.get('specialty').value, 10);

    this.filteredList = this.filteredList.filter((student) => {
      return student.specialtyId === specialty;
    });
  }

  private filterByGroup() {
    const group = parseInt(this.form.get('group').value, 10);

    this.filteredList = this.filteredList.filter((student) => {
      return student.groupId === group;
    });
  }
}
