import { NgModule } from "@angular/core";
import { RoutingModule } from "../routing.module";
import { RouterModule, Routes } from "@angular/router";
import { StudentsListComponent } from "./students-list.component";

const routes: Routes = [
  {
    path: 'students',
    component: StudentsListComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RoutingModule]
})

export class StudentsListRoutingModule {

}
