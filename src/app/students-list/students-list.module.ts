import { StudentsListRoutingModule } from './students-list-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { StudentsListComponent } from './students-list.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    StudentsListRoutingModule
  ],
  declarations: [
    StudentsListComponent
  ]
})
export class StudentsListModule { }
