import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private isLogged = false;

  constructor() { }

  login(): void {
    this.isLogged = true;
  }

  logout(): void {
    this.isLogged = false;
    window.localStorage.clear();
  }

  isUserLogged(): boolean {
    return this.isLogged;
  }
}
