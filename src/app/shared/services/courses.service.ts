import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CoursesService {
  constructor() {}

  getCoursesList(): Number[] {
    return [1, 2, 3, 4];
  }
}
