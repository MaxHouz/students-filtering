import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StudentsService {

  url = 'http://localhost:3000/students';

  constructor(
    private http: HttpClient
  ) {}

  getStudentsList(): Observable<any> {
    return this.http.get(this.url);
  }

}
