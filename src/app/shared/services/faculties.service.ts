import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Student } from '../models/student.model';

@Injectable({
  providedIn: 'root'
})
export class FacultiesService {

  url = 'http://localhost:3000/faculties';

  constructor(
    private http: HttpClient
  ) {}

  getFacultiesList(): Observable<any> {
    return this.http.get(this.url);
  }
}
