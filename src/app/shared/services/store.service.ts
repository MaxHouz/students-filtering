import { CoursesService } from './courses.service';
import { SpecialtiesService } from './specialties.service';
import { GroupsService } from './groups.service';
import { FacultiesService } from './faculties.service';
import { StudentsService } from './students.service';
import { Injectable } from '@angular/core';
import { Student } from '../models/student.model';

@Injectable({
  providedIn: 'root'
})
export class StoreService {

  studentsList: Student[];
  facultiesList: any[];
  groupsList: any[];
  specialtiesList: any[];
  coursesList: any[];

  constructor(
    private studentsService: StudentsService,
    private facultiesService: FacultiesService,
    private groupsService: GroupsService,
    private specialtiesService: SpecialtiesService,
    private coursesService: CoursesService
  ) {
    this.facultiesService.getFacultiesList()
      .subscribe( facultiesList => this.facultiesList = facultiesList);

    this.groupsService.getGroupsList()
      .subscribe( groupsList => this.groupsList = groupsList);

    this.specialtiesService.getSpecialitiesList()
      .subscribe( specialtiesList => this.specialtiesList = specialtiesList);

    this.coursesList = this.coursesService.getCoursesList();
   }

   getFilteredStudentsList() {
    this.studentsService.getStudentsList()
      .subscribe( studentsList => studentsList = studentsList);

     return this.studentsList;
   }
}
