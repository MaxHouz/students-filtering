import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class GroupsService {

  url = 'http://localhost:3000/groups';

  constructor(
    private http: HttpClient
  ) {}

  getGroupsList(): Observable<any> {
    return this.http.get(this.url);
  }
}
