import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../models/user.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  url = 'http://localhost:3000/users';

  constructor(
    private http: HttpClient
  ) {}

  getUserByEmail(email: string): Observable<User> {
    return this.http.get(`${ this.url }?email=${ email }`)
      .pipe(map( (user: User[]) => user[0] ? user[0] : undefined));
  }
   createNewUser(user: User): Observable<User> {
    return this.http.post<User>(this.url, user);
   }
}
