export class Student {
  constructor(
    public id: number,
    public name: string,
    public secondName: string,
    public course: number,
    public facultyId: number,
    public specialtyId: number,
    public groupId: number
  ) {}
}
