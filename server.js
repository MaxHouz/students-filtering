var jsonServer = require('json-server');

var server = jsonServer.create();

server.use(jsonServer.defaults);
server.use('/db1', jsonServer.router('data/faculties.json'));

server.listen(3000);
